// db.fruits.insertMany([
//   {
//     name: "Apple",
//     color: "Red",
//     stock: 20,
//     price: 40,
//     supplier_id: 1,
//     onSale: true,
//     origin: ["Philippines", "US"],
//   },

//   {
//     name: "Banana",
//     color: "Yellow",
//     stock: 15,
//     price: 20,
//     supplier_id: 2,
//     onSale: true,
//     origin: ["Philippines", "Ecuador"],
//   },

//   {
//     name: "Kiwi",
//     color: "Green",
//     stock: 25,
//     price: 50,
//     supplier_id: 1,
//     onSale: true,
//     origin: ["US", "China"],
//   },

//   {
//     name: "Mango",
//     color: "Yellow",
//     stock: 10,
//     price: 120,
//     supplier_id: 2,
//     onSale: false,
//     origin: ["Philippines", "India"],
//   },
// ]);

// [SECTION] MongoDB Aggregation
// used to generate manipulated data and perform operations to create filtered results that helps on analyzing data
// compared to doing CRUD, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary decisions without having to create a frontend application

// Using AGGREGATE METHOD

/* 
$match
-used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process

SYNTAX:
{$match: {field:value}}

*/

db.fruits.aggregate([{ $match: { onSale: true } }]);

/* 
$group
-used to group elements together and field-value pairs, using the data from the group elements

SYNTAX:
	db.collectionName.aggregate([{$group: {_id:"$value", fieldResult: "valueResult"}}])

*/

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

// Aggregating documents using 2 pipeline stages
/*

Syntax:
	db.collectionName.aggregate([
	{$match: {field:value}}, 
	{$group: {_id:"$value", fieldResult: "valueResult"}}

	])


*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

// FIELD PROJECTION with Aggregation
/*
$project
-can be used then aggregating data to include/exclude fields from the returned results

Syntax:
	{$project: {field: 1/0}}

*/

db.fruits.aggregate([{ $project: { _id: 0 } }]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } },
]);

// SORTING AGGREGATED RESULTS
/*
$sort - can be used to change the order of the aggregated results
			- providing a value of -1 will sort the aggregated results in a REVERSE ORDER
			
Syntax:
	{$sort {field: 1/-1}}

*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } },
]);

// Aggregating results based on the array fields
/*
$unwind - deconstructs an array field from a collection with an array value to give us a result for each array elements

{$unwind: field}

*/

db.fruits.aggregate([
  {
    $unwind: "$origin",
  },
]);

// Display fruits by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
  {
    $unwind: "$origin",
  },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
]);
